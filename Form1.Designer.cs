namespace Uebung_09b
{
    partial class Dateien_komprimieren
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmd_end = new System.Windows.Forms.Button();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.cmd_zip = new System.Windows.Forms.Button();
            this.cmd_unzip = new System.Windows.Forms.Button();
            this.cmd_content = new System.Windows.Forms.Button();
            this.cmd_add_1 = new System.Windows.Forms.Button();
            this.cmd_add_2 = new System.Windows.Forms.Button();
            this.cmd_filecheck = new System.Windows.Forms.Button();
            this.cmd_search = new System.Windows.Forms.Button();
            this.folderBrowserDialog_Source = new System.Windows.Forms.FolderBrowserDialog();
            this.folderBrowserDialog_Destination = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFileDialog_Archive = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog_Archive = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(428, 25);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(96, 29);
            this.cmd_end.TabIndex = 0;
            this.cmd_end.Text = "Ende";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // txt_output
            // 
            this.txt_output.Location = new System.Drawing.Point(25, 25);
            this.txt_output.Multiline = true;
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_output.Size = new System.Drawing.Size(364, 334);
            this.txt_output.TabIndex = 1;
            // 
            // cmd_zip
            // 
            this.cmd_zip.Location = new System.Drawing.Point(428, 69);
            this.cmd_zip.Name = "cmd_zip";
            this.cmd_zip.Size = new System.Drawing.Size(96, 26);
            this.cmd_zip.TabIndex = 2;
            this.cmd_zip.Text = "Zippen";
            this.cmd_zip.UseVisualStyleBackColor = true;
            this.cmd_zip.Click += new System.EventHandler(this.cmd_zip_Click);
            // 
            // cmd_unzip
            // 
            this.cmd_unzip.Location = new System.Drawing.Point(428, 111);
            this.cmd_unzip.Name = "cmd_unzip";
            this.cmd_unzip.Size = new System.Drawing.Size(96, 27);
            this.cmd_unzip.TabIndex = 3;
            this.cmd_unzip.Text = "Unzippen";
            this.cmd_unzip.UseVisualStyleBackColor = true;
            this.cmd_unzip.Click += new System.EventHandler(this.cmd_unzip_Click);
            // 
            // cmd_content
            // 
            this.cmd_content.Location = new System.Drawing.Point(428, 156);
            this.cmd_content.Name = "cmd_content";
            this.cmd_content.Size = new System.Drawing.Size(96, 29);
            this.cmd_content.TabIndex = 4;
            this.cmd_content.Text = "Inhalt";
            this.cmd_content.UseVisualStyleBackColor = true;
            this.cmd_content.Click += new System.EventHandler(this.cmd_content_Click);
            // 
            // cmd_add_1
            // 
            this.cmd_add_1.Location = new System.Drawing.Point(428, 202);
            this.cmd_add_1.Name = "cmd_add_1";
            this.cmd_add_1.Size = new System.Drawing.Size(96, 30);
            this.cmd_add_1.TabIndex = 5;
            this.cmd_add_1.Text = "Anhängen 1";
            this.cmd_add_1.UseVisualStyleBackColor = true;
            this.cmd_add_1.Click += new System.EventHandler(this.cmd_add_1_Click);
            // 
            // cmd_add_2
            // 
            this.cmd_add_2.Location = new System.Drawing.Point(428, 248);
            this.cmd_add_2.Name = "cmd_add_2";
            this.cmd_add_2.Size = new System.Drawing.Size(96, 27);
            this.cmd_add_2.TabIndex = 6;
            this.cmd_add_2.Text = "Anhängen 2";
            this.cmd_add_2.UseVisualStyleBackColor = true;
            this.cmd_add_2.Click += new System.EventHandler(this.cmd_add_2_Click);
            // 
            // cmd_filecheck
            // 
            this.cmd_filecheck.Location = new System.Drawing.Point(428, 294);
            this.cmd_filecheck.Name = "cmd_filecheck";
            this.cmd_filecheck.Size = new System.Drawing.Size(96, 27);
            this.cmd_filecheck.TabIndex = 7;
            this.cmd_filecheck.Text = "Filecheck";
            this.cmd_filecheck.UseVisualStyleBackColor = true;
            this.cmd_filecheck.Click += new System.EventHandler(this.cmd_filecheck_Click);
            // 
            // cmd_search
            // 
            this.cmd_search.Location = new System.Drawing.Point(428, 358);
            this.cmd_search.Name = "cmd_search";
            this.cmd_search.Size = new System.Drawing.Size(96, 30);
            this.cmd_search.TabIndex = 8;
            this.cmd_search.Text = "Suchen";
            this.cmd_search.UseVisualStyleBackColor = true;
            this.cmd_search.Click += new System.EventHandler(this.cmd_search_Click);
            // 
            // openFileDialog_Archive
            // 
            this.openFileDialog_Archive.FileName = "openFileDialog1";
            // 
            // Dateien_komprimieren
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 449);
            this.Controls.Add(this.cmd_search);
            this.Controls.Add(this.cmd_filecheck);
            this.Controls.Add(this.cmd_add_2);
            this.Controls.Add(this.cmd_add_1);
            this.Controls.Add(this.cmd_content);
            this.Controls.Add(this.cmd_unzip);
            this.Controls.Add(this.cmd_zip);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.cmd_end);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Dateien_komprimieren";
            this.Text = "Dateien de-/komprimieren";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.Button cmd_zip;
        private System.Windows.Forms.Button cmd_unzip;
        private System.Windows.Forms.Button cmd_content;
        private System.Windows.Forms.Button cmd_add_1;
        private System.Windows.Forms.Button cmd_add_2;
        private System.Windows.Forms.Button cmd_filecheck;
        private System.Windows.Forms.Button cmd_search;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog_Source;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog_Destination;
        private System.Windows.Forms.SaveFileDialog saveFileDialog_Archive;
        private System.Windows.Forms.OpenFileDialog openFileDialog_Archive;
    }
}