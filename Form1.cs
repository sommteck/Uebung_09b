using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;

namespace Uebung_09b
{
    public partial class Dateien_komprimieren : Form
    {
        public Dateien_komprimieren()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Allgemeine Methode zur Dateiauswahl
        private string fktFileDialog(string filePath, char dialogType)
        {
            // Variable für das Ergebnis des Dialogs
            DialogResult dres = DialogResult.None;

            // Variable zurücksetzen
            string file = filePath;

            // OpenFileDialog oder SaveFileDialog anzeigen und Datei wählen
            switch (dialogType)
            {
                case 'o':
                    dres = openFileDialog_Archive.ShowDialog();
                    if (dres == DialogResult.OK)
                        file = openFileDialog_Archive.FileName;
                    break;
                case 's':
                    dres = saveFileDialog_Archive.ShowDialog();
                    if (dres == DialogResult.OK)
                        file = saveFileDialog_Archive.FileName;
                    break;
            }

            //Dateipfad und -namen zurückgeben
            return file;
        }

        // Prozedur zum Packen von Verzeichnissen in ZIP-Archive
        private void cmd_zip_Click(object sender, EventArgs e)
        {
            // Variablen für Quellpfad und Zieldatei
            string srcpath = @"C:\Temp\ZIP-Quelle";
            string dstfile = @"C:\Temp\Archiv.zip";

            // Varaible für Ereignisse der Dialogprozesse
            DialogResult dres;

            // Quellpfad vorbelegen
            folderBrowserDialog_Source.SelectedPath = @"C:\Temp";

            // Quellpfad über Dialog auswählen
            dres = DialogResult.None;
            dres = folderBrowserDialog_Source.ShowDialog();
            if (dres == DialogResult.OK)
            {
                srcpath = folderBrowserDialog_Source.SelectedPath;
            }

            // Zieldatei festlegen
            dres = DialogResult.None;
            dres = saveFileDialog_Archive.ShowDialog();
            if (dres == DialogResult.OK)
            {
                dstfile = saveFileDialog_Archive.FileName;
            }

            txt_output.Text = "Quellpfad: " + srcpath + "\r\n";
            txt_output.Text = txt_output.Text + "Zieldatei: " + dstfile;

            try
            {
                // ZIP-Datei aus Quellverzeichnis erstellen
                ZipFile.CreateFromDirectory(srcpath, dstfile);
            }

            catch
            {
                // Fehlermeldung, wenn Kompression fehlschlägt
                MessageBox.Show("Fehler bei Kompression!");
            }

            finally
            {
                // Meldung, wenn Methode abgearbeitet
                MessageBox.Show("Kompression abgeschlossen!");
            }
        }

        // Prozedur zum Entpacken von ZIP-Archiven
        private void cmd_unzip_Click(object sender, EventArgs e)
        {
            // Variablen für Zielverzeichnis und Quelldatei deklarieren
            string dstpath = @"C:\Temp\ZIP-Ziel";
            string srcfile = @"C:\Temp\Archiv.zip";

            // Wert für Dateifilter setzen
            openFileDialog_Archive.Filter = "ZIP-Dateien|*.zip";

            // Variable für Dialogergebnisse
            DialogResult dres;

            // Archivdatei wählen
            dres = DialogResult.None;
            dres = openFileDialog_Archive.ShowDialog();
            if (dres == DialogResult.OK)
            {
                dstpath = folderBrowserDialog_Destination.SelectedPath;
            }

            txt_output.Text = "Zielpfad: " + dstpath + "\r\n";
            txt_output.Text = txt_output.Text + "Quelldatei: " + srcfile;

            try
            {
                // Dateien in Verzeichnis entpacken
                ZipFile.ExtractToDirectory(srcfile, dstpath);
            }

            catch
            {
                // Fehlermeldung, wenn Entpacken scheitert
                MessageBox.Show("Fehler bei Dekompression!");
            }

            finally
            {
                // Medlung, wenn Methode abgearbeitet
                MessageBox.Show("Dekompression abgeschlossen!");
            }
        }

        // Prozedur zum Auslesen der Archiveinträge
        private void cmd_content_Click(object sender, EventArgs e)
        {
            int i = 0;
            string srcFile = @"C:\Temp\Archiv.zip";
            DialogResult dres;

            dres = openFileDialog_Archive.ShowDialog();
            if (dres == DialogResult.OK)
            {
                srcFile = openFileDialog_Archive.FileName;
            }

            txt_output.AppendText("--- Quelldatei: " + srcFile + " ---\r\n");

            ZipArchive archive = ZipFile.Open(srcFile, ZipArchiveMode.Read);

            foreach (ZipArchiveEntry eintrag in archive.Entries)
            {
                txt_output.AppendText(eintrag + "\r\n");
                i = i + 1;
            }

            archive.Dispose();
            txt_output.AppendText("--- Insgesamt " + Convert.ToInt16(i) + " Einträge im Archiv. ---");
        }

        // Prozedur zum Hinzufügen eines Archiveintreags
        private void cmd_add_1_Click(object sender, EventArgs e)
        {
            // Variablen deklarieren für Quelldatei und OpenFileDialog
            string srcFile = @"C:\Temp\Archiv.zip";
            DialogResult dres;

            // Variable zurücksetzen
            openFileDialog_Archive.FileName = "";

            // OpenFileDialog anzeigen und Datei wählen
            dres = DialogResult.None;
            dres = openFileDialog_Archive.ShowDialog();

            // Falls Datei gewählt wurde, Dateinamen übernehmen; sonst Standard von oben übernehmen
            if (dres == DialogResult.OK)
            {
                srcFile = openFileDialog_Archive.FileName;
            }

            // FileStreanm für ZIP-Datei öffnen
            using (FileStream zipToOpen = new FileStream (srcFile, FileMode.Open))
            {
                // ZIP-Datei öffnen
                using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                {
                    // Zusätzlichen Datei-Eintrag in ZIP-Datei erzeugen
                    ZipArchiveEntry readmeEntry = archive.CreateEntry("Readme.txt");
                    // Zusätzliche Datei schreiben
                    using (StreamWriter writer = new StreamWriter(readmeEntry.Open()))
                    {
                        writer.WriteLine("Dies ist ein Testtext");
                        writer.WriteLine("---------------------");
                    }
                }
                zipToOpen.Dispose();
            }
        }

        private void cmd_add_2_Click(object sender, EventArgs e)
        {
            try
            {
                // Benötigte Variablen deklarieren
                string zipPath = @"C:\Temp\Archiv.zip";
                string newPath = @"C:\Temp\addentry.txt";
                string newFile = "";

                // Archivdatei auswählen
                zipPath = fktFileDialog(zipPath, 'o');

                // Datei zum Hinzufügen auswählen
                newPath = fktFileDialog(newPath, 'o');

                // Dateinamen aus einem Pfad extrahieren
                int position = newPath.LastIndexOf('\\');
                if (position == -1)
                    newFile = newPath;
                else
                    newFile = newPath.Substring(position + 1);

                // Datei dem Archiv hinzufügen
                using (ZipArchive archiv = ZipFile.Open(zipPath, ZipArchiveMode.Update))
                {
                    archiv.CreateEntryFromFile(newPath, newFile);
                    archiv.Dispose();
                }

                txt_output.AppendText("Datei " + newFile + " in ZIP-Datei " + zipPath + " integriert! \r\n");
            }

            catch
            {
                MessageBox.Show("Anhängen von einer oder mehreren Dateien nicht abgeschlossen.");
            }
        }

        private void cmd_filecheck_Click(object sender, EventArgs e)
        {
            // Benötigte Variablen deklarieren
            string zipPath = @"C:\Temp\Archiv.zip";
            string newPath = @"C:\Temp\addentry.tyt";
            string newFile = "";

            zipPath = fktFileDialog(zipPath, 'o');

            // Datei zum Vergleich auswählen
            newPath = fktFileDialog(newPath, 'o');

            // Dateiname aus gesamten Pfad extrahieren
            int position = newPath.LastIndexOf('\\');
            if (position == -1)
                newFile = newPath;
            else
                newFile = newPath.Substring(position + 1);

            // ZIP-Datei zum Lesen öffnen
            ZipArchive archive = ZipFile.Open(zipPath, ZipArchiveMode.Read);

            foreach (ZipArchiveEntry eintrag in archive.Entries)
            {
                if (Convert.ToString(eintrag) == newFile)
                {
                    MessageBox.Show("Datei bereits vorhanden!");
                    return;
                }
            }

            archive.Dispose();
        }

        private void cmd_search_Click(object sender, EventArgs e)
        {
            // Variablen deklarieren für Quelldatei und OpenFileDialog
            int i = 0;
            int j = 0;
            string srcFile = @"C:\Temp\Archiv.zip";
            string srcEntry = "Readme.txt";
            DialogResult dres;

            // Variablen zurücksetzen
            openFileDialog_Archive.FileName = "";

            // OpenFileDialog anzeigen und Datei wählen
            dres = openFileDialog_Archive.ShowDialog();

            // Falls Datei gewählt wurde, Dateinamen übernehmen; sonst Standard von oben übernehmen
            if (dres == DialogResult.OK)
            {
                srcFile = openFileDialog_Archive.FileName;
            }

            using (ZipArchive archiv = ZipFile.Open(srcFile, ZipArchiveMode.Read))
            {
                ZipArchiveEntry entry = archiv.GetEntry(srcEntry);
                if (entry != null)
                    txt_output.AppendText("Datei " + srcEntry + " in " + srcFile + " enthalten! \r\n");
                else
                    txt_output.AppendText("Datei " + srcEntry + " nicht in " + srcFile + " enthalten! \r\n");
            }

            using (ZipArchive archive = ZipFile.Open(srcFile, ZipArchiveMode.Read))
            {
                j = archive.Entries.Count;

                foreach (ZipArchiveEntry ent in archive.Entries)
                {
                    i = i + 1;
                }

                if (i == 0)
                    txt_output.AppendText("Keine Dateien in " + srcFile + " enthalten! \r\n");
                else
                    txt_output.AppendText("Insgesamt " + Convert.ToString(i) + " Dateien in " + srcFile + " enthalten! \r\n");
            }
        }
    }
}